Compile time Conway's Game of Life
==================================

What we're trying to compute (with metaprogramming):

![R Pentomino](reference/r-pentomino.jpg "R Pentomino evolution")

([image source](http://flylib.com/books/en/2.489.1.38/1/))

What we get compiling [hana-life.cpp](./hana-life.cpp) (_Note: no need to execute the binary!_):
```
$ make
clang++-3.7 -std=c++14 -O3 -I../external/hana-0.7.0/include/ -march=native -o hana-life hana-life.cpp
$ strings -w hana-life | grep -C 4 '+++'
PTRh@
UWVS
t$,U

[^_]
+++++
++OO+
+OO++
++O++
+++++

+++++
+OOO+
+O+++
+OO++
+++++

++++++
+++O++
++OO++
+O++O+
++OO++
++++++

++++++
++OO++
++OOO+
+O++O+
++OO++
++++++

++++++
++O+O+
+O++O+
+O++O+
++OO++
++++++

+++++++
+++O+++
+OO+OO+
+O++O++
++OO+++
+++++++

;*2$"
GCC: (Debian 5.3.1-11) 5.3.1 20160307
Debian clang version 3.7.1-2 (tags/RELEASE_371/final) (based on LLVM 3.7.1)
```

Running the executable outputs the images too of course.  But note it contains *no* code to _compute_ them; inspect the assembler (make also creates hana-life.s): just some write calls and associated parameter setups and the static bytes for the string-images above.

6 generations runs (compiles!) in about 15s.  An earlier version tried up to 20 generations needed over two-and-a-half hours and peaked over 5GByte.

TODO 
----

* Code would be cleaner if the various `_functor` classes could be constexpr lambdas, but they're not in until C++17.  I note hana docs are littered with `BOOST_HANA_CONSTEXPR_LAMBDA` which will apply constexpr to various lambdas when it's available.
