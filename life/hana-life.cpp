// Conway's Game of Life, computed at compile time using boost::hana

// Use straight write to stdout rather than C++ iostream to keep built binary/asm as clean as possible
#include <unistd.h> 

#include <boost/hana.hpp>

// Hana docs at http://www.boost.org/doc/libs/master/libs/hana/doc/html/index.html
namespace hana = boost::hana;

// Life rules recap:
//   Any live cell with fewer than two live neighbours dies, as if caused by under-population.
//   Any live cell with two or three live neighbours lives on to the next generation.
//   Any live cell with more than three live neighbours dies, as if by over-population.
//   Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

// Helper function for defining points less verbosely
template <int R,int C> constexpr auto pt() {
  return hana::make_pair(
    hana::int_c<R>,
    hana::int_c<C>
  );
}

// Start with http://www.conwaylife.com/wiki/R-pentomino pattern
constexpr auto r_pentomino=hana::make_set(
             pt<-1,0>(),pt<-1,1>(),
  pt<0,-1>(),pt< 0,0>(),
	     pt< 1,0>()
);

// Neighbouring point offsets
constexpr auto neighbours_offsets=hana::make_tuple(
  pt<-1,-1>(),pt<-1,0>,pt<-1,1>,
  pt< 0,-1>(),pt< 0,0>,pt< 0,1>,
  pt< 1,-1>(),pt< 1,0>,pt< 1,1>
);

 // Coordinate addition helper
struct add_functor {
  template <typename A,typename B> constexpr auto operator()(A a,B b) const {
    return hana::make_pair(
      hana::plus(hana::first(a) ,hana::first(b)),
      hana::plus(hana::second(a),hana::second(b))
    );
  }
};
constexpr auto add=add_functor();

// Generates neighbours of a point INCLUDING the supplied point
struct neighbours_functor {

  template <typename P> constexpr auto operator()(P p) const {

    // Not sure why can't just use p, or even hana::id(p) below
    constexpr auto q=hana::make_pair(hana::first(p),hana::second(p));

    constexpr auto result=hana::make_tuple(
      add(q,pt<-1,-1>()),add(q,pt<-1, 0>()),add(q,pt<-1, 1>()),
      add(q,pt< 0,-1>()),add(q,pt< 0, 0>()),add(q,pt< 0, 1>()),
      add(q,pt< 1,-1>()),add(q,pt< 1, 0>()),add(q,pt< 1, 1>())
    );
    return result;
  }
};
constexpr auto neighbours=neighbours_functor();

////////////////////////////////////////////////////
//
// Calculate some generations
//

// Recursive generation of successive generations:
template <int G> struct generation {

  static constexpr auto previous=generation<G-1>::value;

  struct previously_live_functor {
    template <typename P> constexpr auto operator()(P p) const {
      return hana::contains(previous,p);
    }
  };
  static constexpr auto previously_live=previously_live_functor();

  // Live set expanded to include all neighbours
  static constexpr auto expanded=hana::to_set(
    hana::flatten(
      hana::transform(
	hana::to_tuple(
	  previous
	),
	neighbours
      )
    )
  );

  // Note that this count includes the central cell
  struct neighbours_count_functor {
    template <typename P> constexpr auto operator()(P p) const {
      return hana::length(
	hana::filter(
	  neighbours(p),
	  previously_live
	)
      );
    }
  };
  static constexpr auto neighbours_count=neighbours_count_functor();

  // NB Count includes central cell
  struct survive_condition_functor {
    template <typename P> constexpr auto operator()(P p) const {
      return hana::or_(
	hana::equal(neighbours_count(p),hana::uint_c<3>),
	hana::equal(neighbours_count(p),hana::uint_c<4>)
      );
    }
  };
  static constexpr auto survive_condition=survive_condition_functor();
  
  struct birth_condition_functor {
    template <typename P> constexpr auto operator()(P p) const {
      return hana::equal(
	neighbours_count(p),
	hana::uint_c<3>
      );
    }
  };
  static constexpr auto birth_condition=birth_condition_functor();

  // Construct the next generation
  // Which is the union of unoccupied candidate cells with 3 neighbours and live cells with 3 or 4 neighbours
  template <typename S> static constexpr auto nextgen(S) {

    // Slightly perverse: passing in S s but using previous as using s breaks 
    // If/when constexpr lambdas are in the language, this should be doable a lot more cleanly.
  
    // Unoccupied cell candidates to become live
    constexpr auto t=hana::difference(expanded,previous);
  
    constexpr auto newlife=hana::to_set(hana::filter(hana::to_tuple(t),birth_condition));
    constexpr auto oldlife=hana::to_set(hana::filter(hana::to_tuple(previous),survive_condition));

    constexpr auto result=hana::union_(newlife,oldlife);
    return result;
  }

  static constexpr auto value=nextgen(previous);
};

template <> struct generation<0> {
  static constexpr auto value=r_pentomino;
};

////////////////////////////////////////////////////
//
// Tools for rendering a generation to a string image
//

// Find coordinate ranges
template <typename S> constexpr auto range_rows_lo(S s) {
  return hana::minimum(hana::transform(hana::to_tuple(s),hana::first));  
}
template <typename S> constexpr auto range_rows_hi(S s) {
  return hana::maximum(hana::transform(hana::to_tuple(s),hana::first));  
}
template <typename S> constexpr auto range_rows_size(S s) {
  return hana::plus(hana::minus(range_rows_hi(s),range_rows_lo(s)),hana::int_c<1>);
}

template <typename S> constexpr auto range_cols_lo(S s) {
  return hana::minimum(hana::transform(hana::to_tuple(s),hana::second));  
}
template <typename S> constexpr auto range_cols_hi(S s) {
  return hana::maximum(hana::transform(hana::to_tuple(s),hana::second));  
}
template <typename S> constexpr auto range_cols_size(S s) {
  return hana::plus(hana::minus(range_cols_hi(s),range_cols_lo(s)),hana::int_c<1>);
}

template <int G> struct frame {
  static constexpr auto cells=generation<G>::value;

  static constexpr auto start_row=hana::minus(range_rows_lo(cells),hana::int_c<1>);
  static constexpr auto start_col=hana::minus(range_cols_lo(cells),hana::int_c<1>);

  static constexpr auto rows=hana::plus(range_rows_size(cells),hana::int_c<2>);
  static constexpr auto cols=hana::plus(range_cols_size(cells),hana::int_c<2>);
  static constexpr auto cols_padded=hana::plus(cols,hana::int_c<1>);  // With extra space for '\n'
  static constexpr auto num_pixels=hana::mult(rows,cols_padded);

  template <int STARTROW,int STARTCOL,int COLS> struct pixel_functor {

    // Return the n-th pixel in the image
    template <typename N> constexpr char operator()(N n) const {
      constexpr auto pixel_row=hana::div(n,COLS);
      constexpr auto pixel_col=hana::mod(n,COLS);
      constexpr auto cells_row=hana::plus(STARTROW,pixel_row);
      constexpr auto cells_col=hana::plus(STARTCOL,pixel_col);
      return hana::if_(
	hana::equal(pixel_col,hana::minus(COLS,hana::int_c<1>)),
	hana::char_c<'\n'>,
	hana::if_(
	  hana::contains(cells,hana::make_pair(cells_row,cells_col)),
	  hana::char_c<'O'>,
	  hana::char_c<'+'>
	)
      );
    }
  };

  static constexpr auto render() {
    constexpr auto ns=hana::to_tuple(
      hana::make_range(hana::int_c<0>,num_pixels)
    );
    constexpr auto pixels=hana::transform(
      ns,
      pixel_functor<start_row,start_col,cols_padded>()
    );
    // Put an extra carriage return on the end but NB *not* null-terminated.  
    return hana::append(
      pixels,
      '\n'
    );
  }

  static constexpr auto tupleimage=render();
};

struct getframe_functor {
  template <typename F> constexpr auto operator()(F f) const {
    return frame<f.value>::tupleimage;
  }
};
constexpr auto getframe=getframe_functor();

constexpr auto frames=hana::transform(
  hana::to_tuple(
    hana::make_range(hana::int_c<0>,hana::int_c<6>)  // Control number of frames here
  ),
  getframe
);

template <typename T> inline void print(T t) {
  write(
    1,
    reinterpret_cast<const char*>(&t),
    hana::length(t)
  );
}

int main(int,char**) {
  
  hana::for_each(frames,[](auto f){print(f);});

  return 0;
}
