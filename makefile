all:		external/hana-0.7.0

external/hana-0.7.0:
		mkdir -p external && cd external && wget https://github.com/boostorg/hana/archive/v0.7.0.tar.gz && tar xvfz v0.7.0.tar.gz

.PHONY:		clean

clean:
		rm -r -f external
