constexpr
=========

Daft things with C++ template metaprogramming and constexpr stuff.

Contents
--------

* [brot](./brot) - Mandelbrot set calculation at compile time; constexpr based.
* [life](./life) - Conway's Game of Life using boost::hana metaprogramming.
* [coin](./coin) - Coin-counting combinatorics using boost::hana metaprogramming.

Also

* [experiment](./experiment) - Various bits and pieces testing things out.

Tools
-----

Using:

* `clang++-3.6` (the version in Debian testing/"stretch")
* boost hana <https://github.com/boostorg/hana> : `wget https://github.com/boostorg/hana/archive/v0.7.0.tar.gz`

