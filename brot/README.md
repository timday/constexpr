constexpr-brot
==============

Compile time Mandelbrot set generation using `constexpr`; the result is sitting there in the compiled binary waiting to be output:

```
$ make constexpr-brot-complex
clang++-3.7 -std=c++14 -O3 -I../external/hana-0.7.0/include/ -fconstexpr-steps=1000000000 -march=native -o constexpr-brot-complex constexpr-brot-complex.cpp
$ strings constexpr-brot-complex | grep '+++'
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++O++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OO++++++++++++++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOO+++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOO+++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOOO++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOOO++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOOO++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOO+++++++++++++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++++++++++OO+OOOOOOO+++++++++++O+++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++O++++OOOOOOOOOOOOOOOOOO++++++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++OOO+OOOOOOOOOOOOOOOOOO+++++++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOO+OOO++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOO+OOO++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+O++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++
+++++++++++++++++++++++++++++++++O+++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++
++++++++++++++++++++++++++++OOOOOOOO++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++
++++++++++++++++++++++++++++OOOOOOOOOO++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++
++++++++++++++++++++++++++++OOOOOOOOOOO+++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++
++++++++++++++++++++++++++OOOOOOOOOOOOOO++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++
++++++++++++++++++++++++++OOOOOOOOOOOOOO+OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++
+++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++++
++++++++++++++++++++++OO+OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++++
+++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++++
+OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++++++
+++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++++
++++++++++++++++++++++OO+OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++++
+++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++++
++++++++++++++++++++++++++OOOOOOOOOOOOOO+OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++
++++++++++++++++++++++++++OOOOOOOOOOOOOO++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++
++++++++++++++++++++++++++++OOOOOOOOOOO+++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++
++++++++++++++++++++++++++++OOOOOOOOOO++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++
++++++++++++++++++++++++++++OOOOOOOO++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++
+++++++++++++++++++++++++++++++++O+++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+O++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOO++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOOOOO+++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOO+OOO++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++OOOOOOOOOOOOOOOOOOOOOOOOO+OOO++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++OOO+OOOOOOOOOOOOOOOOOO+++++++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++O++++OOOOOOOOOOOOOOOOOO++++++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++++++++++OO+OOOOOOO+++++++++++O+++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOO+++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOOO++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOOO++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOOO++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOOO+++++++++++++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OOOO+++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++OO++++++++++++++++++++++++++++++++++++
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++O++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
```

Running the executable outputs the same thing, however "success" for the purposes of `constexpr` is of course defined by that output being entirely computed at compile time.  This can be checked by:

* As above, check the executable with `strings`; e.g `strings constexpr-brot | grep '+++'` should also output the image above.
* The assembly output (`.s` files; makefile includes rules to output these) contains a big `.asciz  "++++++++++++++++++++++++++...` initializer for the image above, and nothing to compute it at runtime.

In this directory:

* [`constexpr-brot.cpp`](./constexpr-brot.cpp) uses fixed point integer arithmetic because I thought I might end up templating on actual pixel coordinates.  Compiles in ~15s on an old i7.
* [`constexpr-brot-complex.cpp`](./constexpr-brot-complex.cpp) is much the same but uses `std::complex` (with some minor compromises for lack of `constexpr` on `std::norm` and the complex arithmetic operators).  Compiles in ~5s on an old i7.
