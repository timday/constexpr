#include <iostream>
#include <complex>

////////////////////////////////////////////////////////////////////////////
// A stringy image of the mandelbrot set
// Based off constexpr-brot.cpp but migrated to std::complex
// Could be cleaner if std::norm and arithmetic operators for complex were constexpr

template <int ROWS,int COLS,int MAXITER> struct brot_image {

  // C++14 should allow this as a constexpr constructor
  constexpr brot_image() {
    for (auto i=0;i<ROWS*(COLS+1);i++) {
      rep[i]=pixel_n(i);
    }
    rep[ROWS*(COLS+1)]='\0';
  }
  
  char rep[ROWS*(COLS+1)+1];

  static constexpr double aspect_ratio=double(ROWS)/COLS;

  // Return the n-th pixel in a string-image; includes \n characters
  constexpr char pixel_n(int n) {
    return (n%(COLS+1)==COLS ? '\n' : pixel(n/(COLS+1),n%(COLS+1)));
  }

  // Render a pixel
  static constexpr char pixel(int row,int col) {
    return (
      brot_test(
	std::complex<double>(0.0,0.0),
	pixel_coord(row,col),
	MAXITER
      )
      ? 'O' : '+'
    );
  }

  
  // Complex-plane coordinates of a pixel
  // Zoom in a bit on the usual -2 to +2 range
  static constexpr std::complex<double> pixel_coord(int row,int col) {
    return std::complex<double>
      (
         -2.0+(3.0*col)/(COLS-1),
	(-1.5+(3.0*row)/(ROWS-1))*aspect_ratio
      );
  }

  // Test whether given complex point has failed to escape within a given number of iterations
  static constexpr bool brot_test(std::complex<double> z,std::complex<double> c,unsigned int i) {
    return (
      i==0
      ?
      true
      :
      // Can't use std::norm(std::complex) as not constexpr
      z.real()*z.real()+z.imag()*z.imag()<4.0
      &&
      brot_test(
	// std::complex operator* and operator+ not constexpr either, so need expanding longhand
	std::complex<double>(z.real()*z.real()-z.imag()*z.imag()+c.real(),2.0*z.real()*z.imag()+c.imag()),
	c,
	i-1
      )  
    );
  }
};

constexpr brot_image<67,99,64> img;

int main(int,char**) {
  std::cout << img.rep;
  return 0;
}
