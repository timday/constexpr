#include <iostream>

////////////////////////////////////////////////////////////////////////////
// An 8.24 fixpoint type

struct fixpoint {

  const int val;

  explicit constexpr fixpoint(int v)
    :val(v)
  {}

  explicit constexpr fixpoint(int i,int f)
    :val((i<<24) | f)
  {}
};

constexpr fixpoint operator-(fixpoint a) {
  return fixpoint(-a.val);
}

constexpr fixpoint operator+(fixpoint a,fixpoint b) {
  return fixpoint(a.val+b.val);
}

constexpr fixpoint operator-(fixpoint a,fixpoint b) {
  return fixpoint(a.val-b.val);
}

constexpr fixpoint operator*(fixpoint a,fixpoint b) {
  // 8.24 x 8.24 gets us 16.48 and we want to ditch the bottom 24
  return fixpoint(
    static_cast<int>(
      (
	static_cast<long long int>(a.val)
	*
	static_cast<long long int>(b.val)
      )
      >>24
    )
  );
}

constexpr fixpoint operator/(fixpoint a,fixpoint b) {
  return fixpoint(
    static_cast<int>(
      (
	static_cast<long long int>(a.val)<<24
      )
      /
      b.val
    )
  );
}

constexpr fixpoint muldiv(fixpoint a,fixpoint b,fixpoint c) {
  return fixpoint(
    static_cast<int>(
      (
	static_cast<long long int>(a.val)
	*
	static_cast<long long int>(b.val)
      )
      /c.val
    )
  );
}

constexpr bool operator<(fixpoint a,fixpoint b) {
  return (a.val<b.val);
}

////////////////////////////////////////////////////////////////////////////
// A fixpoint complex type

struct complex {

  const fixpoint real;
  const fixpoint imag;

  constexpr complex(fixpoint r,fixpoint i)
    :real(r)
    ,imag(i)
  {}

  constexpr fixpoint mag2() const {
    return real*real+imag*imag;
  }
};

constexpr complex operator+(complex a,complex b) {
  return complex(
    a.real+b.real,
    a.imag+b.imag
  );
}

constexpr complex operator*(complex a,complex b) {
  return complex(
    a.real*b.real-a.imag*b.imag,
    a.real*b.imag+a.imag*b.real
  );
}

////////////////////////////////////////////////////////////////////////////
// A stringy image of the mandelbrot set

template <int ROWS,int COLS,int MAXITER> struct brot_image {

  // C++14 should allow this as a constexpr constructor
  constexpr brot_image() {
    for (auto i=0;i<ROWS*(COLS+1);i++) {
      rep[i]=pixel_n(i);
    }
    rep[ROWS*(COLS+1)]='\0';
  }
  
  char rep[ROWS*(COLS+1)+1];

  static constexpr fixpoint aspect_ratio=fixpoint(ROWS,0)/fixpoint(COLS,0);

  // Return the n-th pixel in a string-image; includes \n characters
  constexpr char pixel_n(int n) {
    return (n%(COLS+1)==COLS ? '\n' : pixel(n/(COLS+1),n%(COLS+1)));
  }

  // Render a pixel
  static constexpr char pixel(int row,int col) {
    return (
      brot_test(
	complex(fixpoint(0),fixpoint(0)),
	pixel_coord(row,col),
	MAXITER
      )
      ? 'O' : '+'
    );
  }

  
  // Complex-plane coordinates of a pixel
  // Zoom in a bit on the usual -2 to +2 range
  static constexpr complex pixel_coord(int row,int col) {
    return complex
      (
	-fixpoint(2,0    )+muldiv(fixpoint(3,0),fixpoint(col,0),fixpoint(COLS-1,0)),
	(-fixpoint(1,1<<23)+muldiv(fixpoint(3,0),fixpoint(row,0),fixpoint(ROWS-1,0)))*aspect_ratio
      );
  }

  // Test whether given complex point has failed to escape within a given number of iterations
  static constexpr bool brot_test(complex z,complex c,unsigned int i) {
    return (
      i==0
      ?
      true
      :
      z.mag2()<fixpoint(4,0) && brot_test(z*z+c,c,i-1)
    );
  }
};

constexpr brot_image<67,99,64> img;

int main(int,char**) {
  std::cout << img.rep;
  return 0;
}
