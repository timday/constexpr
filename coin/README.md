Coin combo counting
===================

Classic coin counting problem (number of ways of making £2 from british coin denominations).

[hana-coin.cpp](./hana-coin.cpp) compiles quite quickly _if_ the smaller denominations are omitted from the available set; but with all of them it needs ~8 minutes and peaks at almost 3GByte memory consumption on a 64-bit machine.

Needs to be compiled with an increased `-fconstexpr-steps` to avoid a `constexpr evaluation hit maximum step limit; possible infinite loop?` error.

With just a `BOOST_HANA_CONSTANT_CHECK` validation of the computed result, the assembly code's `main:` can be seen to consist of nothing more than a
```
    xorl    %eax, %eax
    retq
```

Reference
---------

In scala, the calculation is

```
def count(denom:List[Int],v:Int):Int = {
  if (denom.length==1)
    {if (v%denom.head==0) 1 else 0}
  else (0 to v/denom.head).map(
    (i:Int)=>count(denom.tail,v-i*denom.head)
  ).sum
}
count(List(200,100,50,20,10,5,2,1),200)
```
(which runs instantly).
