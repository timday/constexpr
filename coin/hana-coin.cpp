#include <iostream>

#include <boost/hana.hpp>

// Hana docs at http://www.boost.org/doc/libs/master/libs/hana/doc/html/index.html
namespace hana = boost::hana;

// Classic coin combinatorics problem (from a well-known problem site):
//   How many ways can £2 be made out of 
//   1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p) coins.

// Not sure if there's a cleaner way of doing this.
struct ways_functor {
  // Number of ways of making value V from denominations D
  template <typename D,typename V> constexpr auto operator()(D d,V v) const;

  // Specialization to avoid infinite recursion
  template <typename V> constexpr auto operator()(const hana::tuple<>&,V v) const {return hana::int_c<0>;}
};
constexpr auto ways=ways_functor();

// Number of ways of making value V from denominations D
template <typename D,typename V> constexpr auto ways_functor::operator()(D d,V v) const {

  return hana::if_(
    hana::equal(hana::length(d),hana::size_c<1>),
    hana::if_(
      hana::equal(hana::mod(v,hana::front(d)),hana::int_c<0>),
      hana::int_c<1>,  // Yes we can make remaining total using last single coin denomination in one way only
      hana::int_c<0>   // No we can't make remaining total using last coin denomination
    ),
    hana::sum<>(
      hana::transform(
	hana::to_tuple(
	  hana::make_range(
	    hana::int_c<0>,
	    hana::int_c<1>+hana::div(v,hana::front(d))  // Choices for number of coins
	  )
	),
	// Build expression for ways(denom.tail,v-denom.head*_)
	hana::compose(  
	  hana::partial(ways,hana::drop_front(d)),
	  hana::partial(hana::minus,v),
	  hana::partial(hana::mult,hana::front(d))
	)
      )
    )
  );
}

constexpr auto denom=hana::make_tuple(
  hana::int_c<200>,
  hana::int_c<100>,
  hana::int_c<50>,
  hana::int_c<20>,
  hana::int_c<10>,
  hana::int_c<5>,
  hana::int_c<2>,
  hana::int_c<1>
);

constexpr auto solution=ways(denom,hana::int_c<200>);

int main(int,char**) {

  static_assert(hana::equal(solution,hana::int_c<73682>),"");

  //BOOST_HANA_RUNTIME_CHECK(solution==hana::int_c<73682>);
  //std::cout << solution << std::endl;

  return 0;
}
