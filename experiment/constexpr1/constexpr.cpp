// Experiment with lambda-alikes

#include <boost/hana.hpp>

namespace hana = boost::hana;

constexpr auto t=hana::make_tuple(
  hana::int_c<1>,
  hana::int_c<2>,
  hana::int_c<3>
);

constexpr auto tgt=hana::make_tuple(
  hana::int_c<2>,
  hana::int_c<4>,
  hana::int_c<6>
);

// What we'd like to do is this:
//   constexpr auto fn0=[](auto x) {return hana::mult(hana::int_c<2>,x);};
//   constexpr auto s0=hana::transform(t,fn0);
// but it doesn't compile because fn0 "must be initialized by a constant expression"
// Presumably lack of constexpr lambda

// This works:
constexpr auto fn2=hana::partial(hana::mult,hana::int_c<2>);
constexpr auto s2=hana::transform(t,fn2);
static_assert(hana::equal(s2,tgt),"");

// This works (but more verbose):
struct s_fn3 {
  template <typename T> constexpr auto operator()(T x) const {
    return hana::mult(hana::int_c<2>,x);
  }
};
constexpr auto fn3=s_fn3();
constexpr auto s3=hana::transform(t,fn3);
static_assert(hana::equal(s3,tgt),"");

int main(int,char**) {
  return 0;
}
