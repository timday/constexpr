// Experiment with hana::string

#include <iostream>
#include "boost/hana.hpp"

namespace hana = boost::hana;

constexpr auto t=hana::make_tuple(
  hana::char_c<'h'>,
  hana::char_c<'e'>,
  hana::char_c<'l'>,
  hana::char_c<'l'>,
  hana::char_c<'o'>,
  hana::char_c<'!'>,
  hana::char_c<'\n'>
);

template <typename T> constexpr auto munge(T t) {
  constexpr auto result=hana::transform(
    t,
    hana::id
  );
  return result;
}

constexpr auto t2=munge(t);

// This works, and can see from error messages it's a
// boost::hana::string<'h', 'e', 'l', 'l', 'o', '!', '\n'>
// as expected
constexpr auto s=hana::unpack(t2,hana::make<hana::string_tag>);
constexpr const char* c=hana::to<const char*>(s);

int main(int,char**) {

  std::cout << c;
  
  return 0;
}
